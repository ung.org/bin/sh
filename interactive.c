/*
 * UNG's Not GNU
 *
 * Copyright (c) 2011-2017, Jakob Kaivo <jkk@ung.org>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#define _XOPEN_SOURCE 700

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>

#ifdef SIGWINCH
#include <sys/ioctl.h>
#endif

#include "sh.h"
#include "shed.h"

sig_atomic_t got_sigwinch = 0;
static char sh_columns[8] = "80";
static char sh_lines[8] = "25";

#ifdef SIGWINCH
static void sh_sigwinch(int sig)
{
	got_sigwinch = 1;
	signal(sig, sh_sigwinch);
}
#endif

void sh_getwinsize(void)
{
	#ifdef TIOCGWINSZ
		struct winsize ws = { 0 };
		ioctl(STDIN_FILENO, TIOCGWINSZ, &ws);
		snprintf(sh_columns, sizeof(sh_columns), "%hhu", ws.ws_col);
		snprintf(sh_lines, sizeof(sh_lines), "%hhu", ws.ws_row);
	#else
		FILE *cols = popen("tput cols", "r");
		if (cols) {
			fread(cols, 1, sizeof(sh_columns), sh_columns);
			fclose(cols);
		}

		FILE *lines = popen("tput lines", "r");
		if (lines) {
			fread(lines, 1, sizeof(sh_lines), sh_lines);
			fclose(lines);
		}
	#endif

	setenv("COLUMNS", sh_columns, 1);
	setenv("LINES", sh_lines, 1);
}

int sh_interactive(void)
{
	struct shed ed = {
		.prompt = getenv("PS1"),
		.handle = shed_handle_non_vi,
	};

	sh_getwinsize();
	#ifdef SIGWINCH
	signal(SIGWINCH, sh_sigwinch);
	#endif

	while (shed(&ed) != NULL) {
		if (ed.cur->nread == 1 && ed.cur->buf[0] == CTRL_D) {
			printf("\n");
			return 0;
		}

		if (strlen(ed.cur->buf) == 0) {
			continue;
		}

		struct command *command = sh_parse(ed.cur->buf);
		if (errno == SIGINT) {
			if (got_sigwinch) {
				sh_getwinsize();
			}
		}

		if (command) {
			sh_execute(command);
			sh_freecmd(command);
		}
	}

	return 0;
}
